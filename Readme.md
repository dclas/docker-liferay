#Docker image for Liferay Community Portal#

This project defines a docker container that includes the Liferay Community Portal.

##Branches##

Choose the appropriate branch for the version of Liferay to be included in the image:

 - 6.0.6 branch for Liferay CE 6.0.6

##Prerequistes##

A working installation of [Docker](http://www.docker.com) (or boot2docker) is required to build this container.

##Usage##

To build the image locally, use the following command to build the Docker container. Alternatively, you can use the autobuild project in the docker hub repository.

````
git checkout x.y.z
docker build -t dclas/liferay:x.y.z .
docker run -P -it dclas/liferay:x.y.z
````
